package main

import (
	"log"

	"github.com/godbus/dbus"
)

func sendNotify(conn *dbus.Conn, msg Message) {
	if conn == nil {
		return
	}

	obj := conn.Object("org.freedesktop.Notifications", "/org/freedesktop/Notifications")

	call := obj.Call(
		// Method
		"org.freedesktop.Notifications.Notify",

		// Flags, optional so left blank (0)
		0,

		// Not sure what this is
		"",

		// This one is probably replace ID
		uint32(0),

		// Self explanatory
		msg.Icon,
		msg.Title,
		msg.Body,

		// Hint and action
		[]string{},
		map[string]dbus.Variant{},

		// Most likely time out, -1 means respecting notification server
		// Unit: millisecond
		int32(5000),
	)

	if call.Err != nil {
		log.Println(call.Err)
	}
}
