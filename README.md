# gobat-cli

The official `gobat` CLI, also written in Go (obviously)

The library doesn't support any operating system other than Linux.

## Features

- Print battery info into terminal
- Output all info as JSON
- Natively send a notification using dbus
- Icon support for battery (normal, full, low, very low)
- Run as a daemon and monitor batteries:
    - When battery status changes (Charging, Full, etc)
    - When battery level reaches below threshold (defineable using flags)

## Installation

### Method 1: Compile

```bash
go get -d gitlab.com/diamondburned/gobat-cli
```

### Method 2: CI

- Go to top
- Find the Download icon
- Download artifact "compile"
- Unzip it, go to `artifacts/`
- Install the binary into `$PATH`

## Command-line options

| Flag    | Type:Default  | Description                                                  |
| ------- | ------------- | ------------------------------------------------------------ |
| `-j`    | `bool:false`  | Print everything as JSON                                     |
| `-v`    | `bool:false`  | Enables verbose logging                                      |
| `-n`    | `bool:false`  | Output status to a notification instead                      |
| `-d`    | `bool:false`  | Daemonize the application and notify                         |
| `-dt`   | `string:"2s"` | Refresh rate, example: `500ms`, only applies when used with `-d` |
| `-dl`   | `int:20`      | Percentage that's considered low, only applies when used with `-d` |
| `-dvl`  | `int:10`      | Percentage that's considered VERY low, only applies when used with `-d` |
| `-ib`   | `string:""`   | Path to battery icon, shows in both `-d` and `-n`            |
| `-ibl`  | `string:""`   | Path to low battery icon, only applies when used with `-d`   |
| `-ibvl` | `string:""`   | Path to very low battery icon, only applies when used with `-d` |
| `-ibf`  | `string:""`   | Path to full battery icon, only applies when used with `-d`  |

## Icon preset

`run.sh` was written with icon presets for `numix`, `papirus` and `adwaita`. 

There are 2 ways to change the preset in the script: 
1. Run it as `GOBAT_CLI_ICON_PRESET="papirus" ./run.sh -n`
2. Edit the variable at line 10 in `run.sh`

#### `.shellrc` method

~~steal~~ Take one of the functions in `run.sh` and put it in `.shellrc`. Here's an example for `~/.bashrc` and Papirus:

```bash
## Add GOBIN to PATH
# PATH="$GOPATH/bin:$PATH"

base="/usr/share/icons/Papirus/symbolic/status/"
icons=(
    ["Battery"]="battery-good-symbolic.svg"
    ["BatteryLow"]="battery-caution-symbolic.svg"
    ["BatteryEmpty"]="battery-empty-symbolic.svg"
    ["BatteryFull"]="battery-full-charged-symbolic.svg"
)

function gobat-cli() {
    $(which gobat-cli) \
        -ib "${base}${icons[Battery]}" \
        -ibf "${base}${icons[BatteryFull]}" \
        -ibl "${base}${icons[BatteryLow]}" \
        -ibvl "${base}${icons[BatteryEmpty]}" \
        $@
}
```

#### If you have a working `$GOPATH`, run 

```bash
(cd $GOPATH/src/gitlab.com/diamondburned/gobat-cli/ && GOBAT_CLI_ICON_PRESET="papirus" ./run.sh -n;)
```

#### If you have the binary in your `$PATH` and the script somewhere, run

```bash
GOBAT_CLI_ICON_PRESET="papirus" ./run.sh -n
```
