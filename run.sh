#!/bin/bash

### HOW-TO
# Run this in your terminal if you have a working GOPATH
#     (cd $GOPATH/src/gitlab.com/diamondburned/gobat-cli/ && ./run.sh;)
# Run this if you have GOBIN in your PATH (you'll need to copy it somewhere)
#     ./run.sh

# Edit me
default=papirus

### ACTUAL CODE (Icons might change in the future. If it does, please make an issue.)

function numix() {
    export base="/usr/share/icons/Numix/scalable/status/"
    export icons=(
        ["Battery"]="battery-good-symbolic.svg"
        ["BatteryLow"]="battery-caution-symbolic.svg"
        ["BatteryEmpty"]="battery-empty-symbolic.svg"
        ["BatteryFull"]="battery-full-charged-symbolic.svg"
    )
}

function papirus() {
    export base="/usr/share/icons/Papirus/symbolic/status/"
    export icons=(
        ["Battery"]="battery-good-symbolic.svg"
        ["BatteryLow"]="battery-caution-symbolic.svg"
        ["BatteryEmpty"]="battery-empty-symbolic.svg"
        ["BatteryFull"]="battery-full-charged-symbolic.svg"
    )
}
 
function adwaita() {
    export base="/usr/share/icons/Adwaita/scalable/status/"
    export icons=(
        ["Battery"]="battery-good-symbolic.svg"
        ["BatteryLow"]="battery-caution-symbolic.svg"
        ["BatteryEmpty"]="battery-empty-symbolic.svg"
        ["BatteryFull"]="battery-full-charged-symbolic.svg"
    )
}

# Check if the var is a thing, if not fallback to the icon defined in L10
[ "$GOBAT_CLI_ICON_PRESET" ] && {
    $GOBAT_CLI_ICON_PRESET &> /dev/null || $default
} || $default

# Check if gobat-cli is a local binary or a command
command -v ./gobat-cli 1> /dev/null || command -v gobat-cli 1> /dev/null && {
    GOBAT_CLI="$_"
}

# Exit if can't find gobat-cli
[ -z "$GOBAT_CLI" ] && exit 1

# Load gobat-cli with the pre-configured icons
$GOBAT_CLI \
    -ib "${base}${icons[Battery]}" \
    -ibf "${base}${icons[BatteryFull]}" \
    -ibl "${base}${icons[BatteryLow]}" \
    -ibvl "${base}${icons[BatteryEmpty]}" \
    $@
