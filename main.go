package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/godbus/dbus"
	"gitlab.com/diamondburned/gobat"
)

// Flags contains all the possible command-line flags
type Flags struct {
	JSON    bool
	Verbose bool
	Notify  bool
	Daemon  DaemonOpts
	Command []string
}

// DaemonOpts contains structs for daemon-related command-line options
type DaemonOpts struct {
	Enable  bool
	Rate    string
	Low     int
	VeryLow int
	Icons   struct {
		Battery      string
		BatteryLow   string
		BatteryEmpty string
		BatteryFull  string
	}
}

// Message #structureEverything
type Message struct {
	Title string
	Body  string
	Icon  string
}

func parseFlags() Flags {
	flags := Flags{}
	dopts := DaemonOpts{}

	flag.BoolVar(&flags.JSON, "j", false,
		"Print everything as JSON")
	flag.BoolVar(&flags.Verbose, "v", false,
		"Enable verbose logging")
	flag.BoolVar(&flags.Notify, "n", false,
		"Output status to a notification instead")

	flag.BoolVar(&dopts.Enable, "d", false,
		"Daemonize the app and notify")
	flag.StringVar(&dopts.Rate, "dt", "2s",
		"Refresh rate, example: `500ms', only applies when used with -d")
	flag.IntVar(&dopts.Low, "dl", 20,
		"Percentage that's considered low, only applies when used with -d")
	flag.IntVar(&dopts.VeryLow, "dvl", 10,
		"Percentage that's considered VERY low, only applies when used with -d")

	flag.StringVar(&dopts.Icons.Battery, "ib", "",
		"Path to battery icon, shows in both -d and -n")
	flag.StringVar(&dopts.Icons.BatteryLow, "ibl", "",
		"Path to low battery icon, only applies when used with -d")
	flag.StringVar(&dopts.Icons.BatteryEmpty, "ibvl", "",
		"Path to very low battery icon, only applies when used with -d")
	flag.StringVar(&dopts.Icons.BatteryFull, "ibf", "",
		"Path to full battery icon, only applies when used with -d")

	flag.Parse()

	flags.Daemon = dopts
	flags.Command = flag.Args()

	return flags
}

var (
	flags = parseFlags()

	// Dbus Global var
	Dbus *dbus.Conn
)

func initDbus() *dbus.Conn {
	conn, err := dbus.SessionBus()
	if err != nil {
		log.Panicln(err)
	}

	return conn
}

func main() {
	if !flags.Verbose {
		log.SetOutput(ioutil.Discard)
	}

	if flags.Daemon.Enable || flags.Notify {
		Dbus = initDbus()
	}

	devices, err := gobat.GetDevices()
	if err != nil {
		log.Panicln(err)
	}

	if err := devices.PopulateAll(); err != nil {
		log.Panicln(err)
	}

	if flags.JSON {
		prnt, err := json.MarshalIndent(devices, "", "\t")
		if err != nil {
			log.Panicln(err)
		}

		fmt.Println(string(prnt))
	}

	if flags.Daemon.Enable {
		c := make(chan os.Signal)
		signal.Notify(c, os.Interrupt, syscall.SIGINT)
		go func() {
			<-c
			_ = Dbus.Close()
			os.Exit(1)
		}()

		rate, err := time.ParseDuration(flags.Daemon.Rate)
		if err != nil {
			log.Fatalln(err)
		}

		throttle := time.Tick(rate)

		oldStatus := make([]string, len(devices.Battery))

		for {
			<-throttle
			for i, b := range devices.Battery {
				if err := b.Populate(); err != nil {
					log.Println(err)
					continue
				}

				if b.Status != oldStatus[i] {
					oldStatus[i] = b.Status
					switch b.Status {
					case "Full":
						sendNotify(Dbus, Message{
							Title: "Battery full",
							Body: fmt.Sprintf("Capacity: %d on battery %s",
								b.Capacity.Capacity, b.Name),
							Icon: flags.Daemon.Icons.BatteryFull,
						})
					case "Unknown":
						continue
					default:
						sendNotify(Dbus, Message{
							Title: b.Status,
							Body: fmt.Sprintf("On battery %s",
								b.Name),
							Icon: flags.Daemon.Icons.Battery,
						})
					}
				} else if b.Capacity.Capacity < flags.Daemon.VeryLow {
					sendNotify(Dbus, Message{
						Title: "Battery very low!",
						Body: fmt.Sprintf("Capacity: %d on battery %s",
							b.Capacity.Capacity, b.Name),
						Icon: flags.Daemon.Icons.BatteryEmpty,
					})
				} else if b.Capacity.Capacity < flags.Daemon.Low {
					sendNotify(Dbus, Message{
						Title: "Battery low",
						Body: fmt.Sprintf("Capacity: %d on battery %s",
							b.Capacity.Capacity, b.Name),
						Icon: flags.Daemon.Icons.BatteryLow,
					})
				} else if b.Capacity.Capacity == 100 {

				}
			}
		}
	}

	// Title is the first line, body is the second
	title, body := "", ""

	for _, b := range devices.Battery {
		title = fmt.Sprintf("Status: %s (%d%%)",
			b.Status, b.Capacity.Capacity,
		)

		// true = Plugged in, false = On battery
		switch devices.AC.Online {
		case true:
			duration, err := b.TimeLeftUntilFull()
			if err != nil {
				log.Panicln(err)
			}

			body = fmt.Sprintf("Time until full charge: %s",
				fmtDuration(*duration),
			)
		case false:
			duration, err := b.TimeRemaining()
			if err != nil {
				log.Panicln(err)
			}

			body = fmt.Sprintf("Time remaining: %s",
				fmtDuration(*duration),
			)
		}
	}

	// if -n
	if flags.Notify {
		sendNotify(Dbus, Message{
			Title: title,
			Body:  body,
			Icon:  flags.Daemon.Icons.Battery,
		})
	} else {
		println(title, "\n"+body)
	}
}

func fmtDuration(d time.Duration) string {
	d = d.Round(time.Minute)
	h := d / time.Hour
	d -= h * time.Hour
	m := d / time.Minute
	return fmt.Sprintf("%d hours %02d minutes", h, m)
}
